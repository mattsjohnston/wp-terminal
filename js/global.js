$(document).ready(function() {

	entries = new Array();
	entry_id = -1;
	cur_page = 0;
	cur_count = 1;
	
	$('#prompt_input').focus()
		.focus(function() {
			$(document).bind('keydown', function(e) {
				if(e.keyCode == 38) nextEntry();
				else if(e.keyCode == 40) previousEntry();
				console.log(entry_id);
			});
		}).blur(function() {
			$(document).unbind('keydown');
		});
	
	$('#prompt').submit(function() {
		var val = $(this).children('#prompt_input').val();
		entries.unshift(val);
		entry_id = -1;
		$(this).children('#prompt_input').val('');
		val = val.split(' ');
		method = val.shift();																					// take the first value as the method name
		flags = new Array();																					// will store all flags here
		options = {};																							// will store all options here
		methods = new Array();																					// will store all method names here (used for help only)
		argument_index = -1;
		$.each(val, function(index, value) {
			if(index != argument_index) {
				if(value.indexOf('--') === 0) flags.push(value.replace("--", ""));
				else if(value.indexOf('-') === 0) {
					options[value.replace("-", "")] = val[index+1];
				}
				else { methods.push(value); }
			}
		});
		console.log([flags, options, methods]);
		commands[method].do(flags, options, methods);
		return false;
	});
	
	commands = {
		help: {
			description: "Displays a list of available commands",
			do: function(flags, options, methods) {
				var output = "";
				for(var i in commands) {
					if(methods.length) i = methods[0];
					var val = commands[i];
					output += "<p>" + i + " - " + val.description + "</p>";
					console.log(val);
					if("flags" in val) {
						output += "<ul>";
						for(var j in val.flags) {
							output += "<li>" + j + ": " + val.flags[j].description + "</li>";
						}
						output += "</ul>";
					}
					if("options" in val) {
						output += "<ul>";
						for(var j in val.options) {
							output += "<li>" + j + ": " + val.options[j].description + "</li>";
						}
						output += "</ul>";
					}
					if(methods.length) break;
				}
				$('#primary').prepend(output);
			}
		},
		clear: {
			description: "Clears the terminal window.",
			do: function(flags, options) {
				$('#primary').empty();
			}
		},
		post: {
			description: "Post handles all the blog posts.",
			flags: {
				"--next": {
                    description: "Retrives the next post which exists in chronological order from the current post.",
                    do: function(options) {
                        console.log('--next');
                        cur_page += 1;
                        return {
                            page: cur_page
                        };
                    }
				},
				"--previous": {
					description: "Retrives the previous post which exists in chronological order from the current post.",
					do: function(options) {
					    cur_page -= 1;
					    return {
					        page: options.page - 1
					    };
					}
				},
				"--first": {
					description: "Retrives the newest post.",
					do: function(options) {
					    cur_page = 0;
					    return {
					        page: 0
					    };
					}
				}
			},
			options: {
				"-count": {
					description: "Specificy a limit on the number of posts to return."
				},
				"-post_id": {
					description: "Specify a post id."
				}
			},
			do: function(flags, options) {
				console.log('post command called');
				var get_options = $.extend({
					count: cur_count,
					page: cur_page
				}, options);
                // console.log(this);
				for(var i = 0; i < flags.length; i++) {
				    console.log([i, flags]);
                    $.extend(get_options, this.flags["--" + flags[i]].do(get_options));
				}
				if(!flags.hasValue('next')) $.extend(get_options, this.flags["--next"].do(get_options));
				$.ajax({
					type: 'GET',
					url: 'http://terminal.dev/api/get_recent_posts/',
					dataType: 'JSON',
					data: get_options,
					success: function(data) {
						var template = Handlebars.compile($('#post_tpl').html());
						$.each(data.posts, function(index, value) {
							var dd = $(template(value));
							$('#primary').prepend(dd);
						});
					}
				});
			}
		}
	};
	
	commands.post.do(new Array(), {});
});

previousEntry = function() {
	if (entry_id > 0) {
		entry_id--;
		$('#prompt_input').val(entries[entry_id]);
	}
	else {
		entry_id = -1;
		$('#prompt_input').val('');
	}
};
nextEntry = function() {
	if (entry_id < entries.length - 1) {
		entry_id++;
		$('#prompt_input').val(entries[entry_id]);
	}
};

Array.prototype.hasValue = function(value) {
  var i;
  for (i=0; i<this.length; i++) { if (this[i] === value) return true; }
  return false;
}

function setSelectionRange(input, selectionStart, selectionEnd) {
  if (input.setSelectionRange) {
    input.focus();
    input.setSelectionRange(selectionStart, selectionEnd);
  }
  else if (input.createTextRange) {
    var range = input.createTextRange();
    range.collapse(true);
    range.moveEnd('character', selectionEnd);
    range.moveStart('character', selectionStart);
    range.select();
  }
}

function setCaretToPos (input, pos) {
  setSelectionRange(input, pos, pos);
}