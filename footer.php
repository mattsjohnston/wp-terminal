<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package Terminal
 * @since Terminal 1.0
 */
?>

	</div><!-- #main -->
</div><!-- #page .hfeed .site -->

<script id="post_tpl" type="text/x-handlebars-template">
	<h1>
		<p class="date">{{date}}</p>
		{{title}}
	</h1>
	<div class="post_body">
		{{{content}}}
	</div>
</script>

<?php wp_footer(); ?>

</body>
</html>